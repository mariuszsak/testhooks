import React, {useState} from 'react';

const SongList = () => {
    const [songs, setSongs] = useState([
        {id: 1, title: 'song one'},
        {id: 2, title: 'song two'}
    ]);
    const addSong = () => {
        setSongs([...songs, {id: 3, title: 'third song'}]);
    }
    return (
        <div>
            <ul>
                <li>song one</li>
                <li>song two</li>
                {songs.map(s => {
                    return (<li key={s.id}>{s.id} x {s.title}</li>)
                })}
            </ul>
            <button onClick={addSong}>add song</button>
        </div>
    );
};

export default SongList;
